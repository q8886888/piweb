#!/usr/bin/env python
#coding:utf8
import web,time
import os
import shelve
import hashlib
import re
render=web.template.render('temp')
urls=(
        '/','index',
        '/login','Login', # 登录验证界面
        '/setnet','SetNet', # 设置网络参数界面 
        '/setnet/setDnsPost','SetDns', 
        '/setnet/setL2tpPost','SetL2tp',
		'/setnet/setYdPost','SetYd',
		'/setnet/setRouterPost','SetRouter',
		'/setnet/setLtPost','SetLt',
        '/login/loginPost','CheckLogin',
        '/change','Change',# 修改登录名和密码界面
        '/change/changePost','ChangePost',
        '/logout','Logout', 
        '/frame/top.html','readTop',
        '/frame/main.html','readMain',
        '/frame/right.html','readRight',
        )
userDbUrl='./userKey.db' # 用户名和密码存放处
def openDb(dbUrl):
    return shelve.open(dbUrl,'c')
def isLogin():
    db=openDb(userDbUrl)
    try:
        username=db['username']
        password=db['password']
    except:
        username='root' # 如果没有保存用户名或密码，则默认是root,root
        password=hashlib.md5('root').hexdigest() # 对密码进行md5密码
    finally:
        db.close()
    try:
        if username==web.cookies().user and password==web.cookies().pwd:
            return True
        else:
            return False
    except:
        return False

def changeKey(user,pwd):
    db=openDb(userDbUrl)
    db['username']=user
    db['password']=pwd
    db.close()

class getSysInfo:
    def getDns(self):
        dnsFile=open('/etc/resolv.conf','r')
        return re.findall(r'\s(.*)\s?',dnsFile.read())
    def getAcc(self):
		l2tpAcc=dict()
		if not isLogin():
			l2tpAcc['lns']=u'登录后可见'
			l2tpAcc['name']=u'登录后可见'
			return l2tpAcc
		l2tpFile=open('/etc/xl2tpd/xl2tpd.conf','r')
		data=l2tpFile.read()
		l2tpAcc['lns']=re.findall(r'lns = (.*)',data)[0]
		l2tpAcc['name']=re.findall(r'name = (.*)',data)[0]
		l2tpFile.close()
		return l2tpAcc
    def getIp(self):
		re_ip=re.compile(r'^(.+?)\s.+?\n\s+?inet addr:(.+?)\s')
		ifData=os.popen('sudo ifconfig').read().split('\n\n')
		ipData=list()
		for eachData in ifData:
			data=re_ip.findall(eachData)
			if data:
				data=data[0]
				ipData.append(data)
		return ipData

