#!/usr/bin/env python
#coding:utf8
from config import *
import time
import os

def setRouter(lns,timeout=0):
		os.popen("sudo route add -host %s dev ppp0"%lns).read()
		os.popen("sudo route del default").read()
		time.sleep(timeout)
		os.popen("sudo route add default ppp1").read()
def conPppoe():
	os.popen("sudo echo 'd net' >/var/run/xl2tpd/l2tp-control").read()
	os.popen('sudo ifconfig eth0 down;sudo ifconfig eth0 up').read()
	os.popen('sudo poff dsl-provider').read()
	os.popen('sudo pon  dsl-provider').read()
def setPppoe(fd):
		dslFd=open('/etc/ppp/peers/dsl-provider','w')
		dslFd.write(fd.read())
		dslFd.close()
		fd.close()
class SetNet:
    def GET(self):
        if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
        return render.setnetwork()
class SetDns:
    def POST(self):
        if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
        data=web.input()
        self.setDns(data.Dns1,data.Dns2)
        web.seeother('/setnet')
    def setDns(self,dns1,dns2):
        if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
        fd=open('/etc/resolv.conf','w')
        if dns1:fd.write('nameserver %s\n'%dns1)
        if dns2:fd.write('nameserver %s\n'%dns2)
        fd.close()

class SetL2tp:
	def POST(self):
		if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
		data=web.input()
		self.setL2tp(data.Lns,data.User,data.Pwd)
		web.seeother('/setnet')
	def setL2tp(self,lns,user,pwd):
		if lns:
			setLns="sudo sed -i 's/lns = .\{0,\}/lns = %s/g' /etc/xl2tpd/xl2tpd.conf"%lns
			os.popen(setLns).read()
		if user:
			setName="sudo sed -i 's/name = .\{0,\}/name = %s/g' /etc/xl2tpd/xl2tpd.conf"%user
			
			setUser="sudo sed -i 's/user.\{0,\}/user \"%s\"/g' /etc/ppp/peers/l2tp.l2tpd"%user
			os.popen(';'.join((setName,setUser))).read()
		if pwd:
			setPwd="sudo sed -i 's/password.\{0,\}/password \"%s\"/g' /etc/ppp/peers/l2tp.l2tpd"%pwd
			os.popen(setPwd).read()
		self.conL2tp(lns)
	
	def conL2tp(self,lns):
		if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
		os.popen("sudo service xl2tpd restart").read()
		os.popen("sudo echo 'd net' >/var/run/xl2tpd/l2tp-control")
		os.popen("sudo echo 'c net' >/var/run/xl2tpd/l2tp-control")
		setRouter(lns,3)

class SetYd:
	def POST(self):
		if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
		ydFd=open('/etc/ppp/peers/yd','r')
		setPppoe(ydFd)
		conPppoe()
		web.seeother('/setnet')
class SetLt:
	def POST(self):
		if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
		ltFd=open('/etc/ppp/peers/lt','r')
		setPppoe(ltFd)
		conPppoe()
		web.seeother('/setnet')
	
class SetRouter:
	def POST(self):
		if not isLogin():raise web.seeother('/login') # 如果用户没有登录，则将页面转到登录界面
		os.popen("route add default ppp1").read()
		web.seeother('/setnet')
