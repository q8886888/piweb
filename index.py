#!/usr/bin/env python
#coding:utf8
from config import *
from login import *
from change import *
from setnet import *
class index:
    def GET(self):
        return render.index()
class readTop:
    def GET(self):
        return render.frame.top()
class readMain:
    def GET(self):
        return render.frame.main()
class readRight:
	def GET(self):
		getInfo=getSysInfo()
		sysInfo={}
		sysInfo['dns']=getInfo.getDns()
		sysInfo['l2tp']=getInfo.getAcc()
		sysInfo['ip']=getInfo.getIp()
		return render.frame.right(sysInfo)
if __name__=='__main__':
    app=web.application(urls,globals())
    app.run()
